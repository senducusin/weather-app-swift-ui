**Weather App [SwiftUI]**

Add and display weather of specific cities

---

## Features

- Add a city
- Change temperature unit to Kelvin, Celsius, or Fahrenheit
- Persistent settings