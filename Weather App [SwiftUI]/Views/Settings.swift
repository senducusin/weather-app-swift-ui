//
//  Settings.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct Settings: View {
    
    @EnvironmentObject var store: Store
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    // User default new syntax for SwiftUI
    @AppStorage("unit") private var selectedUnit: TemperatureUnit = .kelvin
    
    var body: some View {
        
        VStack {
            Picker(selection: $selectedUnit, label: Text("Select temperature unit?"), content: {
                
                ForEach(TemperatureUnit.allCases, id:\.self){
                    Text("\($0.displayText)" as String)
                }
                
            })
            .pickerStyle(SegmentedPickerStyle())
            
            Spacer()
        }
        .padding()
        .navigationTitle("Settings")
        .navigationBarItems(trailing: Button("Done"){
            
            store.selectedUnit = selectedUnit
            mode.wrappedValue.dismiss()
        })
        .embedInNavigationView()
        
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}
