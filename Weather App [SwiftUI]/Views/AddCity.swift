//
//  AddCity.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct AddCity: View {
    
    @EnvironmentObject var store: Store
    @Environment(\.presentationMode) private var presentationMode
    @StateObject private var viewModel = AddWeatherViewModel()
    
    var body: some View {
        
        VStack{
            
            VStack(spacing:20){
                
                TextField("Enter city", text: $viewModel.city)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                
                Button("Save"){
                    viewModel.save { weather in
                        store.addWeather(weather)
                        presentationMode.wrappedValue.dismiss()
                    }
                }
                .padding(10)
                .frame(maxWidth:UIScreen.main.bounds.width/4)
                .foregroundColor(Color.blue)
                .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
                
            }
            .padding()
            .frame(maxWidth: .infinity, maxHeight: 200)
            .background(Color.white)
            .clipShape(RoundedRectangle(cornerRadius: 25.0, style: .continuous))
            
            Spacer()
            
        }
        .padding()
        .navigationTitle("Add City")
        .embedInNavigationView()
        
    }
}

struct AddCity_Previews: PreviewProvider {
    static var previews: some View {
        AddCity()
    }
}
