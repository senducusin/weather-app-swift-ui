//
//  WeatherList.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

enum Sheets: Identifiable {
    case addNewCity, settings
    
    var id: UUID {
        UUID()
    }
}

struct WeatherList: View {
    @EnvironmentObject var store: Store
    @State private var activeSheet: Sheets?
    
    var body: some View {
        List {
            ForEach(store.weatherList, id: \.id) { weather in
                WeatherCell(weather:weather)
            }
        }
        .listStyle(PlainListStyle())
        .sheet(item: $activeSheet, content: {(item) in
            switch item {
            case .addNewCity:
                AddCity().environmentObject(store)
                
            case .settings:
                Settings().environmentObject(store)
            
            }
        })
        .navigationBarItems(leading: Button(action: {
            activeSheet = .settings
        }){
            Image(systemName: "gearshape")
        }, trailing: Button(action: {
            activeSheet = .addNewCity
        }, label:{
            Image(systemName: "plus")
        }))
        .navigationTitle("Cities")
        .embedInNavigationView()
    }
}

struct WeatherList_Previews: PreviewProvider {
    static var previews: some View {
        WeatherList()
    }
}

struct WeatherCell: View {
    @EnvironmentObject var store: Store
    let weather: WeatherViewModel
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 15){
                Text(weather.city)
                    .fontWeight(.bold)
                
                HStack{
                    Image(systemName: "sunrise")
                    Text("\(weather.sunrise.formatAsString())")
                }
                
                HStack{
                    Image(systemName: "sunset")
                    Text("\(weather.sunset.formatAsString())")
                }
            }
            
            Spacer()
            
            ImageUrl(url: WeatherIcon.iconURLOf(icon: weather.icon))
                .frame(width: 50, height: 50)
            
            Text("\(Int(weather.getTemperatureByUnit(unit: store.selectedUnit))) \(String(store.selectedUnit.displayText.prefix(1)))")
        }
        .padding()
        .background((Color.white))
        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
    }
}
