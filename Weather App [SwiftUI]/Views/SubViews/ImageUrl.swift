//
//  ImageUrl.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct ImageUrl: View {
    
    let url: String
    let placeholder: String
    
    @ObservedObject var imageDownloader = ImageDownloaderService()
    
    init(url: String, placeholder: String = "placeholder"){
        self.url = url
        self.placeholder = placeholder
        self.imageDownloader.downloadImage(url: self.url)
    }
    
    var body: some View {
        if let data = imageDownloader.downloadedData,
           let image = UIImage(data: data) {
            return Image(uiImage:image).resizable()
        }else{
            return Image(placeholder).resizable()
        }
    }
}

struct ImageUrl_Previews: PreviewProvider {
    static var previews: some View {
        ImageUrl(url: "https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg")
    }
}
