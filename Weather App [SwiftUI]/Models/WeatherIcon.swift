//
//  WeatherIcon.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

struct WeatherIcon: Codable{
    let main: String
    let description: String
    let icon: String
}

extension WeatherIcon{
    static func iconURLOf(icon: String) -> String {
        return "https://openweathermap.org/img/w/\(icon).png"
    }
}
