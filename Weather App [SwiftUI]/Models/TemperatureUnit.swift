//
//  TemperatureUnit.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

enum TemperatureUnit: String, CaseIterable, Identifiable {
    case celsius
    case fahrenheit
    case kelvin
    
    var id: String {
        return rawValue
    }
}

extension TemperatureUnit {
    var displayText: String {
        self.rawValue.capitalized
    }
}
