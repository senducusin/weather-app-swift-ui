//
//  WebService.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

enum NetworkError: Error {
    case domainError
    case urlError
    case decodeError
}

enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
}

struct Resource<T:Codable>{
    var url: URL
    var httpMethod: HttpMethod = .get
    var httpBody: Data? = nil
}

extension Resource {
    init(url: URL){
        self.url = url
    }
}

class WebService{
    static let API_KEY = "1c3d36af16ecaa4d2191a167d5b652d5"
    
    static let shared = WebService()
    
    func load<T>(resource:Resource<T>, completion:@escaping(Result<T,NetworkError>)->()){
        var request = URLRequest(url: resource.url)
        request.httpMethod = resource.httpMethod.rawValue
        request.httpBody = resource.httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data,_,error in
            guard error == nil,
                  let data = data else {
                completion(.failure(.domainError))
                return
            }
            
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                completion(.success(result))
            }catch{
                completion(.failure(.decodeError))
            }
            
        }.resume()
    }
}
