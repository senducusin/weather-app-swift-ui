//
//  ImageDownloaderService.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

class ImageDownloaderService: ObservableObject {
    @Published var downloadedData: Data?
    
    func downloadImage(url: String){
        guard let imageUrl = URL(string: url) else {return}
        
        URLSession.shared.dataTask(with: imageUrl) { data, _, error in
            guard let data = data, error == nil else {return}
            
            DispatchQueue.main.async {
                self.downloadedData = data
            }
        }.resume()
    }
}
