//
//  View+Extensions.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

extension View {
    func embedInNavigationView() -> some View {
        NavigationView {self}
    }
}
