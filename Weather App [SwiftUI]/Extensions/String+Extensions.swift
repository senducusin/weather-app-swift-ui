//
//  String+Extensions.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

extension String {
    func trimmedAndEscaped() -> String {
        let trimmedString = trimmingCharacters(in: .whitespacesAndNewlines)
        
        return trimmedString
            .addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self
    }
}
