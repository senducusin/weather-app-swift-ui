//
//  WeatherViewModel.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

struct WeatherViewModel {
    let weather: Weather
    
    let id = UUID()
    
    func getTemperatureByUnit(unit:TemperatureUnit) -> Double{
        switch unit {
        case .kelvin:
            return weather.temperature
            
        case .celsius:
            return weather.temperature - 273.15
            
        case .fahrenheit:
            return 1.8 * (weather.temperature - 273) + 32
        }
    }
    
    var temperature: Double {
        weather.temperature
    }
    
    var city: String {
        weather.city
    }
    
    var icon: String {
        weather.icon
    }
    
    var sunrise: Date {
        weather.sunrise
    }
    
    var sunset: Date {
        weather.sunset
    }
}
