//
//  AddWeatherViewModel.swift
//  Weather App [SwiftUI]
//
//  Created by Jansen Ducusin on 5/21/21.
//

import Foundation

class AddWeatherViewModel: ObservableObject {
    var city: String = ""
    
    func save(completion:@escaping( (WeatherViewModel)->()) ){
        
        guard let resource = WeatherResponse.weatherByCity(city: city) else {return}
    
        WebService.shared.load(resource: resource) { result in
            switch result {
            
            case .success(let weatherResponse):
                DispatchQueue.main.async {
                    completion( WeatherViewModel(weather: weatherResponse.weather) )
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
